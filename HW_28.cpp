﻿#include <iostream>

class Animal
{
public:
	virtual void Voice() {}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!\n";
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!\n";
	}
};

class Fish :public  Animal
{
public:
	void Voice() override
	{
		std::cout << "...!\n";
	}
};

class Parrot :public  Animal
{
public:
	void Voice() override
	{
		std::cout << "Sounds imitating speech\n";
	}
};

int main()
{
	int num = 4;
	Animal** animals = new Animal * [num]
	{
		new Dog(),
			new Cat(),
			new Fish(),
			new Parrot()
	};

	for (int i = 0; i < num; i++)
	{
		animals[i]->Voice();
	}
}